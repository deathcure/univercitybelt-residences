<?php
require_once 'includes/header2.php';
?>

<!--==================== MAIN ====================-->
<main class="main">
  <section>
    <div class="container__contact container">
      <h1 class="subscribe__title">Contact Us</h1>
      <p class="subscribe__description">
        Do you have inquiries? Message us.
      </p>
      <form action="index.php" autocomplete="off">
        <div class="input-container">
          <input type="text" name="name" class="input" required />
          <label for="">Name</label>
        </div>

        <div class="input-container">
          <input type="email" name="email" class="input" required />
          <label for="">Email</label>
        </div>
        <div class="input-container">
          <input type="tel" name="phone" class="input" required />
          <label for="">Phone</label>
        </div>
        <div class="input-container textarea">
          <textarea name="message" class="input" required></textarea>
          <label for="">Message</label>
        </div>
        <a href="#" class="button subscribe__button">Send</a>
      </form>
      <p class="subscribe__description">
        <br /><br />For more information, please contact any of the given
        numbers:
        <br />
        <br />JAYROLD JOSE 09364758736 <br />ADRIAN PENAFLOR 09364758736
        <br />
        ANABELLE PENAFLOR 09364758736 <br />
        RICHARD SUPRANES 09364758736 <br />
        MAYETH BANGHULA 09364758736 <br />
      </p>
    </div>
  </section>
</main>

<?php
require_once 'includes/footer.php';
?>