<?php
require_once 'includes/header2.php';
?>

<!--==================== MAIN ====================-->
<main class="main">

  <section class="value section" id="services">
    <div class="value__container container grid">
      <div class="value__images">
        <div class="value__orbe"></div>

        <div class="value__img">
          <img src="assets/img/court.png" alt="lobby" />
        </div>
      </div>

      <div class="value__content">
        <div class="value__data">
          <span class="section__subtitle">ABOUT US</span>
          <h2 class="section__title">
            UniverCity Belt Residences<span>.</span>
          </h2>
          <p class="value__description">
            UniverCity Belt Residences 1985 C.M. Recto Ave., corner S.H. Loyola
            St., Sampaloc, Manila. <br />
            <br />Located at the heart of major colleges and universities in
            Manila. UniverCity Belt Residences<br />
            <br />College & Universities: Far Eastern University (FEU),
            University of the East (UE), University of Sto. Tomas (UST),
            Phili<br />
            <br />Review Centers: Accountancy - PRTC, RESA, CRC-ACE, PAREX, CPAR
            Engineering - MEGA, Besavilla, Padilla, Smart Edge, Review
            Innovations, PRIME Review Center, GERTC, EXCEL Nursing - Top Rank,
            RCAP, SRG, Carl Balita, MERGE, PENTAGON, SRG<br />
            <br />AMENITIES: Study Area (Lights are always ON), FREE Wi-Fi
            (High-speed Fiber), 24/7 Security Guard CCTV, Equipped Lounging
            Area, Basement Parking (with Monthly Dues), Basketball Court, Table
            Tennis, Multi-purpose space for outdoor activities.<br />
            <br />Nearby Establishments: Fastfood Chains (Jollibee, McDonalds,
            KFC, Mang Inasal) - Banking Institutions (Metrobank, BPI, PNB, BDO)
            - Grocery/Retail Stores (Super 8, Ever Supermarket, 7-Eleven,
            Ministop) - Bookstores (National Bookstore, Corona) - Remittance
            Centers (Cebuana Lhuiller, LBC, M Lhuiller)<br />
          </p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="map-container">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d405.83236965638594!2d120.98786698330045!3d14.601928051031768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c9f87df95db9%3A0x19405f3ba74feec6!2sUCB%20Residences!5e0!3m2!1sen!2sph!4v1674189220039!5m2!1sen!2sph" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
  </section>
</main>

<?php
require_once 'includes/footer.php';
?>