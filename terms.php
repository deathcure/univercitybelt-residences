<?php
require_once 'includes/header2.php';
?>

<!--==================== MAIN ====================-->
<main class="main">
  <section class="section">
    <div class="container grid">
      <h2 class="section__title">
        Terms & Agreements <span class="per__person">Room</span>
      </h2>
      <p class="popular__description privacy__text">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci,
        reiciendis dicta ipsam cupiditate doloribus sit ex, aut fugiat
        sapiente excepturi, vero est? Vel explicabo numquam perferendis
        nobis odio. Error cumque natus reiciendis placeat minima amet
        explicabo dolorum quas cum dolore velit distinctio, temporibus vero
        voluptatibus laudantium labore doloribus sint et repellat?
        Laudantium officia dolor perspiciatis amet qui itaque harum facere
        quas ab quisquam optio neque, omnis delectus vel hic similique
        voluptatibus fuga quis aut, praesentium numquam ea eum. Aperiam
        laboriosam dicta ipsam voluptate quos cumque, accusamus doloribus
        distinctio ut perspiciatis quas natus. Quia odit ad expedita
        obcaecati modi architecto provident!
      </p>
      <h2 class="section__title">
        <br />Terms & Agreements <span class="per__person">Transient</span>
      </h2>
      <p class="popular__description privacy__text">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci,
        reiciendis dicta ipsam cupiditate doloribus sit ex, aut fugiat
        sapiente excepturi, vero est? Vel explicabo numquam perferendis
        nobis odio. Error cumque natus reiciendis placeat minima amet
        explicabo dolorum quas cum dolore velit distinctio, temporibus vero
        voluptatibus laudantium labore doloribus sint et repellat?
        Laudantium officia dolor perspiciatis amet qui itaque harum facere
        quas ab quisquam optio neque, omnis delectus vel hic similique
        voluptatibus fuga quis aut, praesentium numquam ea eum. Aperiam
        laboriosam dicta ipsam voluptate quos cumque, accusamus doloribus
        distinctio ut perspiciatis quas natus. Quia odit ad expedita
        obcaecati modi architecto provident!
      </p>
    </div>
  </section>
</main>

<?php
require_once 'includes/footer.php';
?>