-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2023 at 04:04 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `4300899_ucbr`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_tbl`
--

CREATE TABLE `admin_user_tbl` (
  `id` int(150) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `admin_user_tbl`
--

INSERT INTO `admin_user_tbl` (`id`, `username`, `type`, `password`, `dateAdded`) VALUES
(1, 'admin', 'admin', '4297f44b13955235245b2497399d7a93', '2023-04-21 14:03:54'),
(2, 'mrvnmnsld', 'receptionist', '4297f44b13955235245b2497399d7a93', '2023-04-22 16:43:20'),
(3, '0004', 'cashier', 'ceb6c970658f31504a901b89dcd3e461', '2023-04-22 16:45:49'),
(4, 'marvs', 'receptionist', 'ceb6c970658f31504a901b89dcd3e461', '2023-04-23 06:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `chat_history`
--

CREATE TABLE `chat_history` (
  `id` int(150) NOT NULL,
  `adminID` varchar(250) DEFAULT '',
  `userID` varchar(250) DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `chat_history`
--

INSERT INTO `chat_history` (`id`, `adminID`, `userID`, `dateAdded`) VALUES
(1, '2', '12', '2023-04-24 00:38:29'),
(12, '2', '15', '2023-04-24 11:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `chat_msgs`
--

CREATE TABLE `chat_msgs` (
  `id` int(11) NOT NULL,
  `historyID` varchar(255) DEFAULT NULL,
  `sentBy` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `isViewedUser` varchar(255) DEFAULT '0',
  `dateAdded` datetime DEFAULT NULL,
  `isViewedAdmin` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `chat_msgs`
--

INSERT INTO `chat_msgs` (`id`, `historyID`, `sentBy`, `msg`, `isViewedUser`, `dateAdded`, `isViewedAdmin`) VALUES
(1, '1', 'admin', 'Testing6', '1', '2023-04-23 00:39:46', '1'),
(2, '1', 'admin', 'Testing5', '1', '2023-04-23 00:39:46', '1'),
(3, '1', 'user', 'Testing4', '1', '2023-04-23 00:39:46', '1'),
(4, '1', 'user', 'Testing3', '1', '2023-04-23 00:39:46', '1'),
(5, '1', 'admin', 'Testing2', '1', '2023-04-23 00:39:46', '1'),
(6, '1', 'user', 'Testing1', '1', '2023-04-23 00:39:46', '1'),
(7, '1', 'user', 'Testing1', '1', '2023-04-23 00:39:46', '1'),
(8, '1', 'admin', 'Testing1', '1', '2023-04-23 00:39:46', '1'),
(9, '1', 'admin', 'asdasdasd', '1', '2023-04-22 19:54:12', '1'),
(10, '1', 'admin', 'qweqweqweqwe', '1', '2023-04-22 19:54:29', '1'),
(11, '1', 'admin', 'admin send', '1', '2023-04-22 19:55:06', '1'),
(12, '1', 'admin', 'asdasdasd', '1', '2023-04-22 19:57:48', '1'),
(13, '1', 'admin', 'rtesfsdfsdfv\n', '1', '2023-04-22 19:58:13', '1'),
(14, '1', 'user', 'test', '1', '2023-04-22 19:58:36', '1'),
(15, '1', 'user', 'test admin', '1', '2023-04-22 20:00:23', '1'),
(16, '1', 'admin', 'test user\n', '1', '2023-04-22 20:00:30', '1'),
(17, '1', 'admin', 'test@123', '1', '2023-04-22 20:04:16', '1'),
(18, '1', 'user', 'test@123123', '1', '2023-04-22 20:04:25', '1'),
(19, '1', 'user', 'testing@123123123123123123', '1', '2023-04-22 20:04:32', '1'),
(20, '1', 'user', 'test ni admin\n', '1', '2023-04-22 20:05:17', '1'),
(21, '1', 'admin', 'test ni user', '1', '2023-04-22 20:05:22', '1'),
(22, '1', 'user', 'test ni admin asdasdasdasd', '1', '2023-04-22 20:06:15', '1'),
(23, '1', 'admin', 'test ni user asdasdasdasdasdasdasdasd', '1', '2023-04-22 20:06:20', '1'),
(24, '1', 'admin', 'aahahahaha', '1', '2023-04-22 20:06:27', '1'),
(25, '1', 'user', 'heheheheheheheehehehe', '1', '2023-04-22 20:06:31', '1'),
(26, '', 'admin', 'asdasd', '0', '2023-04-23 05:54:18', '0'),
(27, '10', 'admin', 'test', '0', '2023-04-23 06:05:36', '1');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_tbl`
--

CREATE TABLE `reservation_tbl` (
  `id` int(150) NOT NULL,
  `userID` varchar(150) DEFAULT NULL,
  `fbEmail` varchar(150) DEFAULT NULL,
  `fbUsername` varchar(150) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `reviewCenter` varchar(150) DEFAULT NULL,
  `reviewLastSchool` varchar(150) DEFAULT NULL,
  `reviewLastSchoolLocation` varchar(150) DEFAULT NULL,
  `reviewLocation` varchar(150) DEFAULT NULL,
  `course` varchar(150) DEFAULT NULL,
  `yearLevel` varchar(150) DEFAULT NULL,
  `guardianName` varchar(150) DEFAULT '',
  `guardianNetwork` varchar(150) DEFAULT NULL,
  `guardianNumber` varchar(150) DEFAULT NULL,
  `guardianEmail` varchar(150) DEFAULT NULL,
  `guardianRelationship` varchar(150) DEFAULT NULL,
  `guardianAddress` varchar(150) DEFAULT NULL,
  `guardian2Name` varchar(150) DEFAULT NULL,
  `guardian2Network` varchar(150) DEFAULT NULL,
  `guardian2Number` varchar(150) DEFAULT NULL,
  `guardian2Email` varchar(150) DEFAULT NULL,
  `guardian2Relationship` varchar(150) DEFAULT NULL,
  `guardian2Address` varchar(150) DEFAULT NULL,
  `validDocu` varchar(150) DEFAULT NULL,
  `validDocuFile` varchar(150) DEFAULT NULL,
  `roomSelected` varchar(150) DEFAULT NULL,
  `isApprove` varchar(150) DEFAULT '0',
  `checkInDate` varchar(150) DEFAULT '',
  `checkoutDate` varchar(150) DEFAULT '',
  `dateUpdated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `dateAdded` datetime DEFAULT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  `paymentUpload` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `reservation_tbl`
--

INSERT INTO `reservation_tbl` (`id`, `userID`, `fbEmail`, `fbUsername`, `type`, `reviewCenter`, `reviewLastSchool`, `reviewLastSchoolLocation`, `reviewLocation`, `course`, `yearLevel`, `guardianName`, `guardianNetwork`, `guardianNumber`, `guardianEmail`, `guardianRelationship`, `guardianAddress`, `guardian2Name`, `guardian2Network`, `guardian2Number`, `guardian2Email`, `guardian2Relationship`, `guardian2Address`, `validDocu`, `validDocuFile`, `roomSelected`, `isApprove`, `checkInDate`, `checkoutDate`, `dateUpdated`, `dateAdded`, `paymentType`, `paymentUpload`) VALUES
(10, '12', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'Grade 11', 'res@asdasd.com', 'Smart', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'Smart', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', '483808652.pdf', '1', '2', '26 April, 2023', '30 April, 2023', '2023-04-22 15:59:12', '2023-04-22 09:33:59', 'Gcash', '434904248.pdf'),
(11, '14', 'marvin.monsalud.mm@gmail.com', 'res@asdasd.com', 'others', 'Test', 'Adamson', 'Kalayaan', 'QUEZON CITY', 'BSIT', '3rd Year', 'res@asdasd.com', 'Sun', '09611438093', 'marvin.monsalud.mm@gmail.com', 'res@asdasd.com', 'res@asdasd.com', 'res@asdasd.com', 'Globe', '0961 300 2479', 'marvin.monsalud.mm@gmail.com', 'res@asdasd.com', 'res@asdasd.com', '2Ids', '554071717.pdf', '2', '1', '24 April, 2023', '28 April, 2023', '2023-04-23 03:40:26', '2023-04-23 05:38:11', 'Paypal', NULL),
(12, '15', 'johndoe@gmail.com', 'johndoe', 'worker', '', 'aura', 'sbma', '', '', 'Grade 11', 'dad', 'Smart', '12345678909', 'dad@gmail.com', 'father', '01 Sampaloc Street', 'mom', 'Smart', '12345678909', 'mom@gmail.com', 'mother', '01 Sinigang St', 'schoolID', '139477800.jpg', '1', '1', '24 April, 2023', '28 April, 2023', '2023-04-24 10:16:58', '2023-04-24 07:06:36', 'Paypal', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reserve_billing_tbl`
--

CREATE TABLE `reserve_billing_tbl` (
  `id` int(11) NOT NULL,
  `reservationFee` varchar(255) DEFAULT NULL,
  `securityDeposit` varchar(255) DEFAULT NULL,
  `utilityDeposit` varchar(255) DEFAULT NULL,
  `bedSheetDeposit` varchar(255) DEFAULT NULL,
  `aptKey` varchar(255) DEFAULT NULL,
  `roomTypeDeposit` varchar(255) DEFAULT NULL,
  `firstMonthRental` varchar(255) DEFAULT NULL,
  `proRatedRentalDeposit` varchar(255) DEFAULT NULL,
  `reservationID` varchar(255) DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `reserve_billing_tbl`
--

INSERT INTO `reserve_billing_tbl` (`id`, `reservationFee`, `securityDeposit`, `utilityDeposit`, `bedSheetDeposit`, `aptKey`, `roomTypeDeposit`, `firstMonthRental`, `proRatedRentalDeposit`, `reservationID`, `dateAdded`) VALUES
(13, '500', '500', '500', '500', '500', '500', '500', '500', '10', '2023-04-22 09:34:53'),
(14, '1000', '1000', '1000', '1000', '1000', '2000', '2000', '2000', '11', '2023-04-23 05:39:29'),
(16, '1000', '1000', '1000', '1000', '1000', '2000', '0', '1000', '12', '2023-04-24 08:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `rooms_tbl`
--

CREATE TABLE `rooms_tbl` (
  `id` int(150) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `duration` varchar(150) DEFAULT NULL,
  `price` varchar(150) DEFAULT NULL,
  `roomNo` varchar(150) DEFAULT '',
  `dateAdded` timestamp NULL DEFAULT NULL,
  `dateEdited` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rooms_tbl`
--

INSERT INTO `rooms_tbl` (`id`, `description`, `photo`, `type`, `duration`, `price`, `roomNo`, `dateAdded`, `dateEdited`) VALUES
(1, 'Lorem Ipsum', 'room1.jpg', 'Room', 'Monthly', '2500', '502', '2023-04-19 12:07:10', '2023-04-22 09:51:38'),
(2, 'Lorem Ipsum', 'room2.jpg', 'Bedspace', 'Transient', '3500', '208', '2023-04-19 12:07:11', '2023-04-22 09:51:44'),
(3, '123123', '787111601.png', '123', '123', '123', '123', '2023-04-22 04:14:41', NULL),
(4, 'This is a room beri gud', '440843993.png', '123123', '123123', '52000', '123123123', '2023-04-22 04:15:12', NULL),
(5, 'Good nice room veri nice', '422647070.png', 'Room', 'Monthly', '4500', '810', '2023-04-22 22:14:44', '2023-04-24 07:51:34');

-- --------------------------------------------------------

--
-- Table structure for table `room_checkout_tbl`
--

CREATE TABLE `room_checkout_tbl` (
  `id` int(250) NOT NULL,
  `returnItems` varchar(250) DEFAULT NULL,
  `refunds` varchar(250) DEFAULT NULL,
  `unpaid` varchar(250) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `reservationID` varchar(250) DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL,
  `isApprove` varchar(250) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room_checkout_tbl`
--

INSERT INTO `room_checkout_tbl` (`id`, `returnItems`, `refunds`, `unpaid`, `note`, `reservationID`, `dateAdded`, `isApprove`) VALUES
(1, 'asdasdasd', '123123', '123', '123', '10', '2023-04-22 14:16:02', '2'),
(2, 'keys', '123', '123', '123123123', '12', '2023-04-24 13:01:04', '1');

-- --------------------------------------------------------

--
-- Table structure for table `room_maintainance_tbl`
--

CREATE TABLE `room_maintainance_tbl` (
  `id` int(200) NOT NULL,
  `reservationID` varchar(200) DEFAULT NULL,
  `reason` varchar(200) DEFAULT '',
  `isApprove` varchar(200) DEFAULT '0',
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room_maintainance_tbl`
--

INSERT INTO `room_maintainance_tbl` (`id`, `reservationID`, `reason`, `isApprove`, `dateAdded`) VALUES
(5, '10', '123123123123', '1', '2023-04-22 13:37:57'),
(6, '10', 'a sfasd asd asd as da sd asd', '2', '2023-04-22 13:55:05'),
(7, '12', 'wsssd', '0', '2023-04-24 12:36:55');

-- --------------------------------------------------------

--
-- Table structure for table `room_monthly_bills_tbl`
--

CREATE TABLE `room_monthly_bills_tbl` (
  `id` int(150) NOT NULL,
  `reservationID` varchar(150) DEFAULT NULL,
  `water` varchar(150) DEFAULT NULL,
  `electricity` varchar(150) DEFAULT NULL,
  `rent` varchar(150) DEFAULT NULL,
  `others` varchar(150) DEFAULT NULL,
  `paymentType` varchar(150) DEFAULT NULL,
  `paymentUpload` varchar(150) DEFAULT NULL,
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room_monthly_bills_tbl`
--

INSERT INTO `room_monthly_bills_tbl` (`id`, `reservationID`, `water`, `electricity`, `rent`, `others`, `paymentType`, `paymentUpload`, `dateAdded`) VALUES
(5, '10', '500', '500', '500', '500', 'Paypal', NULL, '2023-04-22 09:44:52'),
(8, '11', '1000', '1000', '1000', '1000', 'Gcash', '298243196.txt', '2023-04-23 06:13:36'),
(9, '12', '300', '500', '2500', '0', 'Paypal', NULL, '2023-04-24 08:45:57');

-- --------------------------------------------------------

--
-- Table structure for table `room_transfer_tbl`
--

CREATE TABLE `room_transfer_tbl` (
  `id` int(200) NOT NULL,
  `reservationID` varchar(200) DEFAULT NULL,
  `roomNo` varchar(200) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `isApprove` varchar(200) DEFAULT '0',
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room_transfer_tbl`
--

INSERT INTO `room_transfer_tbl` (`id`, `reservationID`, `roomNo`, `note`, `isApprove`, `dateAdded`) VALUES
(5, '10', '123123123', '123123123123', '2', '2023-04-22 13:37:57'),
(6, '11', '508', 'This is a tesrt', '2', '2023-04-23 06:15:32'),
(7, '12', '100', 'yaw ko dito', '1', '2023-04-24 12:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `id` int(155) NOT NULL,
  `fullname` varchar(155) DEFAULT '',
  `email` varchar(155) DEFAULT NULL,
  `password` varchar(155) DEFAULT NULL,
  `phoneNumber` varchar(155) DEFAULT NULL,
  `bday` varchar(155) DEFAULT '',
  `gender` varchar(155) DEFAULT '',
  `dateAdded` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`id`, `fullname`, `email`, `password`, `phoneNumber`, `bday`, `gender`, `dateAdded`) VALUES
(12, 'Mark Villar 2', 'marvin.monsalud.mm@gmail.com', '4297f44b13955235245b2497399d7a93', '0961 300 2479', '2023-04-05', 'male', '2023-04-19 10:48:47'),
(13, 'John Doe', 'test@123.com', '4297f44b13955235245b2497399d7a93', NULL, '', '', '2023-04-23 05:33:42'),
(14, 'John Does', 'john@gmail.com', 'ceb6c970658f31504a901b89dcd3e461', '09613002479', '2023-04-19', 'male', '2023-04-23 05:36:13'),
(15, 'John Doe', 'johndoe@gmail.com', '4297f44b13955235245b2497399d7a93', '09613002479', '2023-04-25', 'female', '2023-04-24 06:45:26'),
(16, 'This Test', 'test@test.com', '4297f44b13955235245b2497399d7a93', '091234567890', '2023-04-24', 'female', '2023-04-24 06:51:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user_tbl`
--
ALTER TABLE `admin_user_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_history`
--
ALTER TABLE `chat_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_msgs`
--
ALTER TABLE `chat_msgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation_tbl`
--
ALTER TABLE `reservation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserve_billing_tbl`
--
ALTER TABLE `reserve_billing_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms_tbl`
--
ALTER TABLE `rooms_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_checkout_tbl`
--
ALTER TABLE `room_checkout_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_maintainance_tbl`
--
ALTER TABLE `room_maintainance_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_monthly_bills_tbl`
--
ALTER TABLE `room_monthly_bills_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_transfer_tbl`
--
ALTER TABLE `room_transfer_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user_tbl`
--
ALTER TABLE `admin_user_tbl`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chat_history`
--
ALTER TABLE `chat_history`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `chat_msgs`
--
ALTER TABLE `chat_msgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `reservation_tbl`
--
ALTER TABLE `reservation_tbl`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reserve_billing_tbl`
--
ALTER TABLE `reserve_billing_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `rooms_tbl`
--
ALTER TABLE `rooms_tbl`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_checkout_tbl`
--
ALTER TABLE `room_checkout_tbl`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `room_maintainance_tbl`
--
ALTER TABLE `room_maintainance_tbl`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `room_monthly_bills_tbl`
--
ALTER TABLE `room_monthly_bills_tbl`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `room_transfer_tbl`
--
ALTER TABLE `room_transfer_tbl`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `id` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
