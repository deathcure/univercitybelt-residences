<?php
require_once 'includes/header.php';
require_once 'includes/sidebar.php';
require_once 'includes/navbar.php';
require_once 'includes/chatbox.php';
?>

<!--********************************** CONTENT BODY START ***********************************-->
<section>
  <div class="content-body">
    <div class="container-fluid">
      <!-- row -->
      <iframe src="./assets/pdf-files/01-UCBR-Contract-of-Lease.pdf" width="100%" height="800px">
        <p>
          Your browser does not support PDFs.
          <a href="./assets/pdf-files/01-UCBR-Contract-of-Lease.pdf">Download the PDF</a>
          to view it.
        </p>
      </iframe>
    </div>
  </div>
</section>
<!--********************************** CONTENT BODY END ***********************************-->

<?php
require_once 'includes/footer.php';
?>