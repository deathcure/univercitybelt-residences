<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="robots" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="UniverCity Belt Residences" />
  <meta property="og:title" content="UniverCity Belt Residences" />
  <meta property="og:description" content="UniverCity Belt Residences" />
  <meta property="og:image" content="./images/white_logo.png" />
  <meta name="format-detection" content="telephone=no" />

  <!-- ================ ICO IMAGE ================ -->
  <link rel="icon" type="image/x-icon" href="./images/white_logo.ico" />

  <!-- PAGE TITLE HERE -->
  <title>Admin Login</title>

  <!-- FAVICONS ICON -->
  <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
  <link href="vendor/jquery-nice-select/css/nice-select.css" rel="stylesheet" />
  <link href="vendor/owl-carousel/owl.carousel.css" rel="stylesheet" />
  <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />

  <!-- Style css -->
  <link href="css/style.css" rel="stylesheet" />
</head>

<body class="vh-100">
  <div class="authincation h-100">
    <div class="container h-100">
      <div class="row justify-content-center h-100 align-items-center">
        <div class="col-md-6">
          <div class="authincation-content">
            <div class="row no-gutters">
              <div class="col-xl-12">
                <div class="auth-form">
                  <h4 class="text-center mb-4">Sign In</h4>
                  <form action="index.php">
                    <div class="mb-3">
                      <label class="mb-1"><strong>Email</strong></label>
                      <input type="email" class="form-control" value="" placeholder="hello@example.com" />
                    </div>
                    <div class="mb-3">
                      <label class="mb-1"><strong>Password</strong></label>
                      <input type="password" class="form-control" value="Password" />
                    </div>
                    <div class="row d-flex justify-content-between mt-4 mb-2">
                      <div class="mb-3">
                        <div class="form-check custom-checkbox ms-1">
                          <input type="checkbox" class="form-check-input" id="basic_checkbox_1" />
                          <label class="form-check-label" for="basic_checkbox_1">Remember me</label>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-primary btn-block">
                        Sign In
                      </button>
                    </div>
                    <div class="mb-3 mt-3 text-center">
                      <a href="forgot_password.php">Forgot Password?</a>
                    </div>
                  </form>
                  <div class="new-account mt-3 text-center">
                    <p>Don't have an account? Contact administration.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--**********************************
        Scripts
    ***********************************-->
  <!-- Required vendors -->
  <script src="vendor/global/global.min.js"></script>
  <script src="js/custom.min.js"></script>
  <script src="js/dlabnav-init.js"></script>
</body>

</html>