<!--********************************** SIDEBAR HEADER ***********************************-->
<nav>
    <div class="nav-header">
        <a href="index.php" class="brand-logo">
            <img src="./images/white_logo.png" alt="logo" width="45" height="45" viewbox="0 0 45 45" fill="none" />

            <div class="brand-title">
                <h2 class="">UCBR</h2>
                <span class="brand-sub-title">@UniverCityBeltResidences</span>
            </div>
        </a>
        <div class="nav-control">
            <div class="hamburger">
                <span class="line"></span><span class="line"></span><span class="line"></span>
            </div>
        </div>
    </div>

    <!-- SIDEBAR START ***********************************-->
    <div class="dlabnav">
        <div class="dlabnav-scroll">
            <ul class="metismenu" id="menu">
                <li data-type="all">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-sharp fa-solid fa-message"></i>
                        <span class="nav-text">Messages</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="message.php">Chat</a>
                        </li>
                    </ul>
                </li>

                <li data-type="receptionist">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-address-card"></i>
                        <span class="nav-text">Registration</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="registration_users_page.php">Registered Users</a>
                        </li>
                    </ul>
                </li>

                <li data-type="cashier">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-money-bill"></i>
                        <span class="nav-text">Transactions</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="transaction_billing.php">Transactions Billing</a>
                        </li>
                    </ul>
                </li>

                <li data-type="receptionist">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-door-closed"></i>
                        <span class="nav-text">Rooms</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="room_available.php">Rooms</a>
                        </li>

                        <li>
                            <a href="room_maintenance.php">Maintenance Request</a>
                        </li>

                        <li>
                            <a href="room_checkout.php">Checkout Request</a>
                        </li>

                        <li>
                            <a href="room_transfer.php">Transfer Request</a>
                        </li>

                    </ul>
                </li>

                <li data-type="cashier">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fas fa-file-alt"></i>
                        <span class="nav-text">Letter Templates</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="letter_reservation_agreement.php">Reservation Agreement Letter</a>
                        </li>
                        <li>
                            <a href="letter_contract.php">Contract of Lease Letter</a>
                        </li>
                        <li>
                            <a href="letter_rules_and_regulations.php">Rules and Regulations Letter</a>
                        </li>
                        <li>
                            <a href="letter_contract_renewal.php">Contract Renewal Letter</a>
                        </li>
                    </ul>
                </li>

                <li data-type="admin">
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-gear"></i>
                        <span class="nav-text">System Users</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="admin_list.php">Admin List</a></li>
                        <li><a href="user_list.php">User List</a></li>
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--********************************** SIDEBAR END ***********************************-->
