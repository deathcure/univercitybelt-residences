<?php
require_once 'includes/header2.php';
?>

<!--==================== MAIN ====================-->
<main class="main">
  <section class="section">
    <div class="container grid">
      <h2 class="section__title">Privacy Policy</h2>
      <p class="popular__description privacy__text">
        Rent monthly. Minimum of 3 months. Will share room with other
        people. Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Eius optio ullam minima ratione aliquid facere dignissimos,
        reiciendis, voluptatum sit dolore eligendi ea id culpa excepturi ab
        delectus. Quia magnam, quaerat quod odit inventore molestiae.
        Quaerat impedit eum nemo necessitatibus, modi fugiat odit molestiae
        illum beatae praesentium doloribus aperiam at saepe dolorem?
        Repellendus quod provident itaque ipsum aspernatur nulla maiores
        deserunt numquam doloremque magni natus, velit cumque cupiditate
        tempora asperiores, placeat porro autem. Pariatur aliquam culpa
        quas. Nisi accusantium autem odio amet ex, nulla a voluptatum
        mollitia aliquid ipsum, consectetur iste blanditiis. A maiores
        harum, quas autem provident, ea itaque, eaque tempora velit iusto
        voluptates nostrum reiciendis nam excepturi repellat. Impedit
        consequuntur quas optio. Dolore molestias repellat qui ad expedita
        illum perspiciatis iure voluptatum maiores quis inventore, sit minus
        dignissimos id reiciendis quaerat? Omnis, enim? Veniam eligendi eos
        voluptas. Suscipit voluptas totam ratione perferendis atque dicta?
        Quo accusamus aliquid temporibus voluptatem nam id enim hic, a ad
        adipisci soluta nemo pariatur tempora atque velit possimus dolor
        excepturi earum numquam in voluptatibus corporis eveniet
        perspiciatis officia? Reiciendis veritatis qui eaque architecto
        natus adipisci quis delectus sequi eos quos quisquam nulla,
        doloribus, cum totam hic blanditiis voluptates deleniti? Ipsa ex
        rerum modi optio explicabo autem odit nostrum nulla dolorem
        voluptatibus soluta quidem eligendi nobis incidunt similique, alias
        deleniti laboriosam sapiente aut? Sequi autem natus, optio maiores
        quas distinctio, et dolorem consequuntur vero amet, beatae sapiente
        minus laudantium dignissimos. Dolorem, fugit? Ipsa obcaecati
        asperiores molestiae atque, esse, similique molestias in veniam
        nihil, illo minima tempore explicabo temporibus? Deleniti sint fuga
        nesciunt? Ullam ut, numquam tenetur vel perspiciatis blanditiis
        quibusdam maiores ea dolores architecto rem at aliquid dicta itaque,
        quis inventore officia, eos consequuntur doloribus? Aliquid tempora,
        maiores veniam ab impedit totam dignissimos eius eos id iste?
        Eveniet quia sapiente corporis placeat vero cum, nihil tenetur
        numquam aperiam dolore beatae repellat? Excepturi commodi sapiente
        rerum placeat? Officia consequuntur placeat quam at nesciunt
        similique tempore eius et minima reiciendis unde, aut, amet aperiam
        nihil, optio ad ut cumque quaerat esse. Debitis, voluptas nisi quos
        delectus ipsum suscipit soluta, temporibus, modi placeat dolor eaque
        laborum. Atque magnam, enim rerum esse expedita veniam tenetur quis
        corrupti eos, assumenda ratione impedit! Possimus qui molestiae
        molestias dolores doloribus dignissimos assumenda consectetur
        temporibus eius! Sed quam aut omnis quos dolor earum, ullam mollitia
        sunt iusto, nostrum rem cum? Quod, quam veritatis, in, alias quae ea
        assumenda minima eum similique dolorem modi iure earum mollitia
        culpa eos. Eligendi eaque doloribus debitis explicabo suscipit
        itaque, perspiciatis quod ut. Vitae impedit eveniet molestiae
        voluptate velit aliquam laudantium mollitia assumenda ad dolor non
        est magnam, nihil expedita beatae atque placeat iusto repellat,
        eligendi harum. Cumque commodi atque dolores totam repellat facilis
        ipsum veniam! At quasi officiis veniam laudantium, hic et
        perspiciatis iste quo laborum autem quos odio deleniti expedita
        saepe eligendi debitis tempore dignissimos adipisci. Rem
        consequuntur voluptas itaque dolorem corrupti, repellendus velit
        aperiam reiciendis ea at fugiat, minima, voluptate illo animi? Iste
        nulla, dolores sit itaque minima quibusdam recusandae, velit
        blanditiis cupiditate aspernatur quam?
      </p>
    </div>
  </section>
</main>

<?php
require_once 'includes/footer.php';
?>