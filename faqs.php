<?php
require_once 'includes/header2.php';
?>

<!--==================== MAIN ====================-->
<main class="main">

  <!--==================== SERVICES ====================-->
  <section class="value section" id="services">
    <div class="value__container container grid">
      <div class="faqs__images">
        <div class="faqs__orb"></div>

        <div class="faqs__img">
          <img src="assets/img/faqs.png" alt="question mark" />
        </div>
      </div>

      <div class="value__content">
        <div class="value__data">
          <h2 class="section__title">
            Frequently Asked Questions<span>.</span>
          </h2>
        </div>

        <div class="value__accordion">
          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-shield-x value__accordion-icon"></i>
              <h3 class="value__accordion-title">Best room locations</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-x-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">Comfortable environment</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-bar-chart-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">
                Affordable price on the market
              </h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-check-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">Apartment's Security</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-shield-x value__accordion-icon"></i>
              <h3 class="value__accordion-title">Best room locations</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-x-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">Comfortable environment</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-bar-chart-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">
                Affordable price on the market
              </h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>

          <div class="value__accordion-item">
            <header class="value__accordion-header">
              <i class="bx bxs-check-square value__accordion-icon"></i>
              <h3 class="value__accordion-title">Apartment's Security</h3>
              <div class="value__accordion-arrow">
                <i class="bx bxs-down-arrow"></i>
              </div>
            </header>

            <div class="value__accordion-content">
              <p class="value__accordion-description">
                We are always ready to help you by providing the best service
                just for you. We believe a good place to stay will make your
                stay easier.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<?php
require_once 'includes/footer.php';
?>