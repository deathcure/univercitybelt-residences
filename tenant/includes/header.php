<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="UniverCity Belt Residences" />
    <meta property="og:title" content="UniverCity Belt Residences" />
    <meta property="og:description" content="UniverCity Belt Residences" />
    <meta property="og:image" content="./images/white_logo.png" />
    <meta name="format-detection" content="telephone=no" />

    <!-- ================ ICO IMAGE ================ -->
    <link rel="icon" type="image/x-icon" href="./images/white_logo.ico" />

    <!-- PAGE TITLE HERE -->
    <title>Profile</title>

    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <link href="vendor/jquery-nice-select/css/nice-select.css" rel="stylesheet" />
    <link href="vendor/owl-carousel/owl.carousel.css" rel="stylesheet" />
    <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />

    <!-- For Calendar Information -->
    <link href="vendor/fullcalendar/css/main.min.css" rel="stylesheet" />

    <!-- Daterange picker -->
    <link href="vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <!-- Clockpicker -->
    <link href="vendor/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet" />
    <!-- asColorpicker -->
    <link href="vendor/jquery-asColorPicker/css/asColorPicker.min.css" rel="stylesheet" />
    <!-- Material color picker -->
    <link href="vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Pick date -->
    <link rel="stylesheet" href="vendor/pickadate/themes/default.css" />
    <link rel="stylesheet" href="vendor/pickadate/themes/default.date.css" />
    <link href="../icon.css?family=Material+Icons" rel="stylesheet" />

    <!-- Style css -->
    <link href="css/style.css" rel="stylesheet" />
</head>

<body>
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <!--**********************************