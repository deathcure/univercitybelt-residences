<!-- ************************************* Nav header start ***********************************-->
<nav>
    <div class="nav-header">
        <a href="index.php" class="brand-logo">
            <img src="./images/white_logo.png" alt="logo" width="45" height="45" viewbox="0 0 45 45" fill="none" />

            <div class="brand-title">
                <h2 class="">UCBR</h2>
                <span class="brand-sub-title">@UniverCityBeltResidences</span>
            </div>
        </a>
        <div class="nav-control">
            <div class="hamburger">
                <span class="line"></span><span class="line"></span><span class="line"></span>
            </div>
        </div>
    </div>

    <!-- Sidebar start ***********************************-->
    <div class="dlabnav">
        <div class="dlabnav-scroll">
            <ul class="metismenu" id="menu">
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-sharp fa-solid fa-message"></i>
                        <span class="nav-text">Messages</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="message.php">Chat</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-address-card"></i>
                        <span class="nav-text">Registration</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="reservation_form.php">Registration Form</a>
                        </li>

                        <!-- <li>
                            <a href="reservation_billing.php">Registration Billing</a>
                        </li>
                        <li>
                            <a href="reservation_payment.php">Registration Payment</a>
                        </li> -->
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-money-bill"></i>
                        <span class="nav-text">Transactions</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="transaction_billing.php">Billings</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-receipt"></i>
                        <span class="nav-text">Invoices</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="invoice.php">Monthly Invoice</a>
                        </li>

                        <li>
                            <a href="reservation_invoice.php">Reservation Invoice</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fa-solid fa-door-closed"></i>
                        <span class="nav-text">Rooms</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a href="room_transfer.php">Transfer Request</a>
                        </li>

                        <li>
                            <a href="room_checkout.php">Checkout Request</a>
                        </li>

                        <li>
                            <a href="room_maintenance.php">Maintenance Request</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!--********************************** Sidebar end ***********************************-->
</nav>
<!--************************************* Nav header end **************************************-->