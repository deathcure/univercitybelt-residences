<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- ================ ICO IMAGE ================ -->
    <link rel="icon" type="image/x-icon" href="assets/img/white_logo.ico" />
    <!--=============== BOXICONS ===============-->
    <link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet" />
    <!--=============== SWIPER CSS ===============-->
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css" />

    <!--=============== CSS ===============-->
    <link rel="stylesheet" href="assets/css/styles.css" />

    <title>UniverCity Belt Residences</title>
</head>

<body>
    <!--==================== HEADER ====================-->
    <header class="header scroll-header" id="header">
        <nav class="nav container">
            <a href="./index.php" class=" nav__logo">
                UCBR <img src="assets/img/white_logo.png" class="nav__img" alt="" />
            </a>

            <!-- Theme change button -->
            <i class="bx bx-moon change-theme" id="theme-button"></i>
        </nav>
    </header>