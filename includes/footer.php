<!--==================== FOOTER ====================-->
<footer class="footer section">
    <div class="footer__container container grid">
        <div>
            <a href="#" class="footer__logo">
                UCBR
                <img src="assets/img/white_logo.png" class="nav__img" alt="logo" />
            </a>
            <p class="footer__description">
                Our vision is to make all people <br />
                live in a place where it's comfortable.
            </p>
        </div>

        <div class="footer__content">
            <div>
                <h3 class="footer__title">About</h3>

                <ul class="footer__links">
                    <li>
                        <a href="./aboutus.php" class="footer__link">About Us</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/ucbresidences" class="footer__link">News & Blogs</a>
                    </li>
                </ul>
            </div>

            <div>
                <h3 class="footer__title">Support</h3>

                <ul class="footer__links">
                    <li>
                        <a href="./faqs.php" class="footer__link">FAQs</a>
                    </li>
                    <li>
                        <a href="./contact.php" class="footer__link">Contact Us</a>
                    </li>
                </ul>
            </div>

            <div>
                <h3 class="footer__title">Follow us</h3>

                <ul class="footer__social">
                    <a href="https://www.facebook.com/ucbresidences" target="_blank" class="footer__social-link">
                        <i class="bx bxl-facebook-circle"></i>
                    </a>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer__info container">
        <span class="footer__copy"> &#169; UCBR. All rights reserved </span>

        <div class="footer__privacy">
            <a href="./terms.php">Terms & Agreements</a>
            <a href="./privacy.php">Privacy Policy</a>
        </div>
    </div>
</footer>

<!--========== SCROLL UP ==========-->
<a href="#" class="scrollup" id="scroll-up">
    <i class="bx bxs-chevrons-up"></i>
</a>

<!--=============== SCROLLREVEAL ===============-->
<script src="./assets/js/scrollreveal.min.js"></script>

<!--=============== SWIPER JS ===============-->
<script src="./assets/js/swiper-bundle.min.js"></script>

<!--=============== MAIN JS ===============-->
<script src="./assets/js/main.js"></script>
<script src="./assets/js/contact.js"></script>
<script src="https://cdn.jsdelivr.net/npm/toastify-js@1.9.3/toastify.min.js"></script>
</body>

</html>